﻿cmake_minimum_required (VERSION 3.15)

project ("DIEngine")

message("CMAKE_SOURCE_DIR:${CMAKE_SOURCE_DIR}")
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/thirdparty/Catch2/contrib)

add_subdirectory(thirdparty)

add_subdirectory(obj)

#enable_testing()

add_subdirectory(engine)
